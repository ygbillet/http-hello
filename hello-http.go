package main // import "bitbucket.org/ygbillet/hello-http"

import (
	"crypto/tls"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/mavricknz/ldap"
)

func indexHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "It's working\n")
	fmt.Fprintf(w, "Host:"+r.Host+"\n")
	fmt.Fprintf(w, "X-Forwarded-For :"+r.Header.Get("X-FORWARDED-FOR")+"\n")
	fmt.Fprintf(w, "X-Real-Ip :"+r.Header.Get("X-REAL-IP")+"\n")
	fmt.Fprintf(w, "X-Forwarded-Proto :"+r.Header.Get("X-FORWARDED-PROTO")+"\n")
	log.Println("-------------------------------------")
	log.Println("Start Reverse Proxy Header Inspection")
	log.Printf("X-Forwarded-For : %s\n", r.Header.Get("X-FORWARDED-FOR"))
	log.Printf("X-Forwarded-Proto : %s\n", r.Header.Get("X-FORWARDED-PROTO"))
	log.Println("-------------------------------------")
}

func helloHandler(w http.ResponseWriter, r *http.Request) {
	remPartOfURL := r.URL.Path[len("/hello/"):] //get everything after the /hello/ part of the URL
	fmt.Fprintf(w, "Hello %s!", remPartOfURL)
}

func ldapHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		fmt.Fprint(w, `<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style>
	label {display:block; padding-bottom:5px;}
	</style>
</head>
<body>
<form action="/ldap/" method="post">
	<label for="server">IP:<input name="server" id="server" type="text" /></label>
	<label for="port">Port:<input name="port" id="port" type="text" /></label>
	<label for="ssl">SSL:<input name="ssl" id="ssl" type="checkbox" /></label>
	<label for="insecure">Disable Cert Check:<input name="insecure" id="insecure" type="checkbox" /></label>
	<label for="username">DN:<input name="username" id="username" type="text" /></label>
	<label for="password">Password:<input name="password" id="password" type="password" /></label>
	<br />
	<input type="submit" />
</form>
</body>
</html>`)
	} else {
		var l *ldap.LDAPConnection
		var ssl string
		var connected, insecure bool
		connect := "Connection to LDAP server : <b style='color:red'>Failed</b><br />"
		result := "Bind to LDAP : <b style='color:red'>Failed</b>"
		fmt.Fprint(w, "<!DOCTYPE html><html><head><title></title></head><body>")
		server := r.FormValue("server")
		username := r.FormValue("username")
		password := r.FormValue("password")
		port, err := strconv.ParseInt(r.FormValue("port"), 0, 16)
		if err != nil {
			fmt.Fprintf(w, "Cannot convert port %s\n", err.Error())
			return
		}
		if r.FormValue("insecure") == "on" {
			insecure = true
		}

		log.Println("--------------------------")
		log.Println("Start LDAP Connection Test")
		log.Printf("  Server : %s:%v\n", server, port)
		log.Printf("  SSL : %s\n", ssl)
		log.Printf("  Username : %s\n", username)
		log.Printf("  Password : %s\n", username)

		if r.FormValue("ssl") == "on" {
			ssl = "on"
			l = ldap.NewLDAPSSLConnection(server, uint16(port), &tls.Config{InsecureSkipVerify: insecure})
			log.Printf("  InsecureSkipVerify %v", insecure)
		} else {
			ssl = "off"
			l = ldap.NewLDAPConnection(server, uint16(port))
		}

		err = l.Connect()
		if err == nil {
			log.Printf("  Connection to LDAP Server successful")
			connect = "Connection to LDAP server : <b style='color:green'>OK</b><br />"
			connected = true
		} else {
			log.Printf("  Cannot connect to LDAP Server : %s\n", err)
		}
		fmt.Fprint(w, connect)

		if connected {
			err = l.Bind(username, password)

			if err == nil {
				log.Printf("  Binding to LDAP Server successful")
				result = "Bind to LDAP : <b style='color:green'>OK</b>"
			} else {
				log.Printf("  Cannot bind to LDAP Server : %s\n", err)
			}
		}

		l.Close()
		log.Println("--------------------------")
		fmt.Fprint(w, result)
		fmt.Fprint(w, "</body></html>")
	}
}

func main() {
	log.Println("Running on 0.0.0.0:9999")
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/hello/", helloHandler)
	http.HandleFunc("/ldap/", ldapHandler)
	http.ListenAndServe("0.0.0.0:9999", nil)
}
