# README #

An Hello World HTTP Server in Go.
I use it as placeholder for testing reverse-proxy configuration or firewall rules.

You can use the following docker image : https://hub.docker.com/r/ygbillet/http-hello/

## Usage ##

This server listen on 0.0.0.0:9999.
In other words it listen on all interface, waiting for HTTP connection on
TCP port 9999.

Available URL are :

- `/`
- `/hello/<username>/`
- `/ldap/`

Path `/` check following HTTP headers :

    X-Forwarded-To
    X-Real-Ip
    X-Forwarded-Proto

Path `/hello/<username>/`

Path `/ldap/` check LDAP connection

## How do I get set up? ##

### Using Docker ####

1. Pull image from Docker Hub : `docker pull ygbillet/http-hello`
2. For listening on host port 8081 : `docker run -d -p 8081:9999 ygbillet/http-hello`

### Using source ###

1. Download and install Go: https://golang.org/dl/
2. `go get bitbucket/ygbillet/http-hello`
3. `go build`


## Dependencies ##

* Go (1.4+) : http://www.golang.org/
* Go Librairies :
	- github.com/mavricknz/asn1-ber https://github.com/mavricknz/asn1-ber master ce3c3d1d8e4190a390fc72f551222783cbd6e55a
	- github.com/mavricknz/ldap     https://github.com/mavricknz/ldap     master 6638977d26a6bc5ac0d2a9a5dcbefe02c456ea38